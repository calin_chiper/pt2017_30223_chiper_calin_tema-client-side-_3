import { MarketplaceClientPage } from './app.po';

describe('marketplace-client App', () => {
  let page: MarketplaceClientPage;

  beforeEach(() => {
    page = new MarketplaceClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
