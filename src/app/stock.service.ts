import { Injectable } from '@angular/core';
import {Http, Headers} from "@angular/http";
import {Stock} from "./products/Stock";
import {Observable} from "rxjs/Observable";

@Injectable()
export class StockService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private stockURL = "http://localhost:8080/stock/";

  constructor(private http: Http) { }

  pushStockData(stock: Stock): Observable<Stock> {
    return this.http.post(this.stockURL, JSON.stringify(stock), {headers: this.headers}).map(
      (response) => response.json()
    );
  }

  fetchStockData(): Observable<Stock[]> {
    return this.http.get(this.stockURL).map(
      (response) => response.json()
    );
  }

  deleteStockData(stock: Stock): Observable<Stock> {
    let jsonObj = JSON.stringify(stock);
    return this.http.delete(this.stockURL, {headers: this.headers, body: jsonObj}).map(
      (response) => response.json()
    );
  }

  updateStockData(stock: Stock): Observable<Stock> {
    return this.http.put(this.stockURL, JSON.stringify(stock), {headers: this.headers}).map(
      (response) => response.json()
    );
  }
}
