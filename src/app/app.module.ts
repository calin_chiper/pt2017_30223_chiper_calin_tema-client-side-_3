import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CustomersComponent } from './customers/customers.component';
import { ProductsComponent } from './products/products.component';
import { HistoryComponent } from './history/history.component';
import {APP_ROUTES_PROVIDER} from "./app.routes";
import { OrderCreatorComponent } from './order-creator/order-creator.component';
import { HomeComponent } from './home/home.component';
import { CustomerService } from "./customer.service";
import {StockService} from "./stock.service";
import {PurchaseService} from "./purchase.service";

@NgModule({
  declarations: [
    AppComponent,
    CustomersComponent,
    ProductsComponent,
    HistoryComponent,
    OrderCreatorComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    APP_ROUTES_PROVIDER
  ],
  providers: [CustomerService, StockService, PurchaseService],
  bootstrap: [AppComponent]
})
export class AppModule { }
