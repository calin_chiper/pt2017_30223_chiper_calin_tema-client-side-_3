import { Component, OnInit } from '@angular/core';
import {Purchase} from "../order-creator/purchase";
import {PurchaseService} from "../purchase.service";

@Component({
  selector: 'history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  purchases: Purchase[];

  constructor(private purchaseService: PurchaseService) { }

  getPurchaseHistory() {
    this.purchaseService.fetchPurchaseData().subscribe(
      (data) => this.purchases = data
    );
  }

  ngOnInit() {
    this.getPurchaseHistory();
  }

}
