import { Component, OnInit } from '@angular/core';
import {Purchase} from "./purchase";
import {Customer} from "../customers/customer";
import {Stock} from "../products/Stock";
import {CustomerService} from "../customer.service";
import {StockService} from "../stock.service";
import {PurchaseService} from "../purchase.service";
import 'rxjs/add/operator/catch';



@Component({
  selector: 'app-order-creator',
  templateUrl: './order-creator.component.html',
  styleUrls: ['./order-creator.component.css']
})
export class OrderCreatorComponent implements OnInit {
  purchase: Purchase;
  customers: Customer[];
  stock: Stock[];
  submitted: boolean;
  invalid: boolean;
  price: number;

  constructor(private customerService: CustomerService, private stockService: StockService, private purchaseService: PurchaseService) {
    this.purchase = new Purchase();
  }

  getAllCustomers() {
    this.customerService.fetchCustomersData().subscribe(
      (data) => this.customers = data
    );
  }

  getStock() {
    this.stockService.fetchStockData().subscribe(
      (data) => this.stock = data
    )
  }

  onSubmit(purchase: Purchase, isValid: boolean) {
      if(isValid) {
        this.submitted = true;
        this.purchase = purchase;
        this.price = purchase.product.price * purchase.quantity;
        this.purchaseService.pushPurchaseData(purchase).subscribe(
          (data) => {
            purchase = new Purchase()
          },
          (error) => {
            this.submitted = false;
            this.invalid = true;
          }
        );
      }
  }

  ngOnInit() {
    this.getAllCustomers();
    this.getStock();
    this.submitted = false;
    this.invalid = false;
  }

}
