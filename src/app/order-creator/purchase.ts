

import {Product} from "../products/Product";
import {Customer} from "../customers/customer";

export class Purchase {
  id: number;
  customer: Customer;
  product: Product;
  purchaseDate: Date;
  quantity: number;

  constructor() {
    this.customer = new Customer();
    this.product = new Product();

  }
}
