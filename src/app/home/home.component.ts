import { Component, OnInit } from '@angular/core';
import {TEXT} from "./welcome-text";

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  welcomeText: string;
  constructor() { }

  ngOnInit() {
    this.welcomeText = TEXT;
  }

}
