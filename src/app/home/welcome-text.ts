

export const TEXT = "Welcome to Warehouse manager web application." +
  "This app was developed to track warehouse products easily. Its purpose is to increases productivity and efficiency." +
  "You can choose from the menu above what to track or to manage. " +
  "This web app uses an API for fetching and pushing data. The next paragraph is used to fill this text section:" +
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
  "Nulla fermentum felis eget suscipit porta. Quisque nec commodo nibh. " +
  "Pellentesque ornare scelerisque ante. Fusce quis libero at augue interdum aliquam. " +
  "Vestibulum nec elit massa. Mauris pharetra molestie velit, ac vestibulum diam gravida efficitur." +
  " Proin finibus nunc tristique blandit faucibus. In scelerisque ante diam, id posuere orci viverra at" +
  "Mauris nec velit ex. Vestibulum nunc libero, suscipit gravida felis quis, accumsan sagittis elit. Integer eget egestas mi. " +
  "Nullam id odio risus. Etiam at massa eget erat bibendum laoreet. Nullam et lacus at metus placerat posuere id ac nulla. " +
  "In efficitur purus ornare diam maximus, convallis dignissim tellus aliquet. Curabitur in sem sed odio cursus aliquet." +
  " Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas eu neque ut sapien tempor finibus. " +
  "Duis condimentum sodales elit in finibus. Maecenas sodales blandit augue aliquam tempus. " +
  "Ut magna turpis, feugiat nec placerat ac, placerat eu augue.";
