import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Purchase} from "./order-creator/purchase";
import {Http, Headers} from "@angular/http";


@Injectable()
export class PurchaseService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private purchaseURL = "http://localhost:8080/order/";
  private historyURL = "http://localhost:8080/history/";

  constructor(private http: Http) { }


  pushPurchaseData(purchase: Purchase): Observable<Purchase> {
      return this.http.post(this.purchaseURL, JSON.stringify(purchase), {headers: this.headers}).map(
        (response) =>  response.json()
      );
  }

  fetchPurchaseData(): Observable<Purchase[]> {
    return this.http.get(this.historyURL, {headers: this.headers}).map(
      (response) => response.json()
    );
  }

}
