import { Injectable } from '@angular/core';
import {Headers, Http} from "@angular/http";
import 'rxjs/add/operator/map';
import {Observable} from "rxjs/Observable";
import {Customer} from "./customers/customer";
import {Router} from "@angular/router";

@Injectable()
export class CustomerService {
  private headers = new Headers({'Content-Type': 'application/json'});
  private customersURL = "http://localhost:8080/customers/";

  constructor(private http: Http) { }

  fetchCustomersData(): Observable<Customer[]> {
    return this.http.get(this.customersURL).map(
      (response) => response.json()
    );
  }

  updateCustomerData(customer: Customer): Observable<Customer> {
      return this.http.put(this.customersURL, JSON.stringify(customer), {headers: this.headers}).map(
        (response) => response.json()
      );
  }

  pushCustomerData(customer: Customer): Observable<Customer> {
    return this.http.post(this.customersURL, JSON.stringify(customer), {headers: this.headers}).map(
      (response) => response.json()
    );

  }

  removeCustomerData(customer: Customer): Observable<Customer> {
    let jsonObj = JSON.stringify(customer);
    return this.http.delete(this.customersURL, {headers: this.headers, body: jsonObj}).map(
      (response) => response.json()
    );
  }

}
