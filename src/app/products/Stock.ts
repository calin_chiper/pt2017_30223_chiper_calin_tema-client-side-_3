
import {Product} from "./Product";
export class Stock {
  product: Product;
  totalQuantity: number;

  constructor() {
    this.product = new Product();
  }
}
