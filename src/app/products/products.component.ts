import { Component, OnInit } from '@angular/core';
import {CATEGORIES} from "./category-list";
import {Stock} from "./Stock";
import {StockService} from "../stock.service";


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  categories: string[] = CATEGORIES;
  stockList: Stock[];
  newStock: Stock;
  selectedStock: Stock;

  constructor(private stockService: StockService) {
    this.newStock = new Stock();
  }

  setSelectedStock(stock: Stock) {
    this.selectedStock = stock;
  }

  addNewProduct() {
    this.stockService.pushStockData(this.newStock).subscribe(
      (data) => {
        this.newStock = new Stock();
        this.getAllProducts();
      }
    )
  }

  deleteSelectedProduct() {
    this.stockService.deleteStockData(this.selectedStock).subscribe(
      (data)=> {
        this.selectedStock = null;
        this.getAllProducts();
      }
    )
  }

  getAllProducts() {
    this.stockService.fetchStockData().subscribe(
      (data) => this.stockList = data
    )
  }

  updatePriceAndQuantity() {
    this.stockService.updateStockData(this.selectedStock).subscribe(
      (data) => {
        this.selectedStock = null;
        this.getAllProducts();
      }
    )
  }

  ngOnInit() {
    this.getAllProducts();
  }

}
