
export class Product {
  id: number;
  productName: string;
  category: string;
  description: string;
  price: number;
}
