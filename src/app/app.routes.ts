
import {RouterModule} from "@angular/router";
import {CustomersComponent} from "./customers/customers.component";
import {HistoryComponent} from "./history/history.component";
import {ProductsComponent} from "./products/products.component";
import {OrderCreatorComponent} from "./order-creator/order-creator.component";
import {HomeComponent} from "./home/home.component";

const APP_ROUTES = [
  {path: 'customers', component: CustomersComponent},
  {path: 'history', component: HistoryComponent},
  {path: 'stock', component: ProductsComponent},
  {path: 'order', component: OrderCreatorComponent},
  {path: '', component: HomeComponent}
];

export const APP_ROUTES_PROVIDER = RouterModule.forRoot(APP_ROUTES);
