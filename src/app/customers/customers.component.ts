import { Component, OnInit } from '@angular/core';
import { Customer } from './customer';
import { CustomerService} from '../customer.service';


@Component({
  selector: 'customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})


export class CustomersComponent implements OnInit {
  customers: Customer[];
  selectedCustomer: Customer;
  newCustomer: Customer;
  submitted: boolean;

  constructor(private customerService: CustomerService) {
    this.newCustomer = new Customer();
  }

  setSelectedCustomer(customer: Customer) {
    this.selectedCustomer = customer;
  }

  updateSelectedCustomer() {
    this.customerService.updateCustomerData(this.selectedCustomer).subscribe(
      (data) => {
        this.selectedCustomer = data;
        this.selectedCustomer = null;
      }
    );
  }

  removeSelectedCustomer() {
    this.customerService.removeCustomerData(this.selectedCustomer).subscribe(
      (data) => {
        for(let index = 0; index < this.customers.length; index++) {
          if(this.selectedCustomer.id === this.customers[index].id) {
            this.customers.splice(index, 1);
            break;
          }
        }
        this.selectedCustomer = data;
        this.selectedCustomer = null;
      }
    );
  }

  addNewCustomer() {
    this.customerService.pushCustomerData(this.newCustomer).subscribe(
      (data) => {
        this.customers.push(data);
        this.getAllCustomers();
        this.newCustomer = new Customer();
      }
    );
  }

  getAllCustomers() {
    this.customerService.fetchCustomersData().subscribe(
      (data) => this.customers = data
    );
  }


  ngOnInit() {
    this.getAllCustomers();
  }

}
